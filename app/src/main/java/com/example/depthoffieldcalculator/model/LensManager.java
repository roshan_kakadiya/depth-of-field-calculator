package com.example.depthoffieldcalculator.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class LensManager implements Iterable<Lens> {

    private static LensManager instance;
    private List<Lens> lens1 = new ArrayList<>();

    public static LensManager getInstance(){
        if (instance == null){
            instance = new LensManager();
        }
        return instance;
    }

    @Override
    public Iterator<Lens> iterator(){
        return lens1.iterator();
    }

    public void remove(int index){
        lens1.remove(index);
    }
    public void add(Lens lens2){
        lens1.add(lens2);
    }
    public List<Lens> getLens() {
        return lens1;
    }


}
