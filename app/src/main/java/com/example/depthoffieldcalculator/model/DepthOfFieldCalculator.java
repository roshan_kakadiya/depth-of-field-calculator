package com.example.depthoffieldcalculator.model;

public class DepthOfFieldCalculator {


    private double Aperture;
    private double Distance;
    private double COC;
    private double NFP;
    private double FFP;
    private Lens lens;

    public DepthOfFieldCalculator(Lens lens, double Distance, double Aperture, double circleOfConfusion ){
        this.Aperture = Aperture;
        this.Distance = Distance;
        this.lens = lens;
        this.COC = circleOfConfusion;
    }

    public void setUserEnteredAperture(double Aperture) {
        this.Aperture = Aperture;
    }

    public void setUserEnteredDistanceToSubject(double Distance) {
        this.Distance = Distance;
    }

    public void setCOC(double COC) {
        this.COC = COC;
    }

    public double getCOC() {
        return COC;
    }

    public double getUserEnteredAperture() {
        return Aperture;
    }

    public double getUserEnteredDistanceToSubject() {
        return Distance;
    }

    public double hyperFocalDistanceCalculator(DepthOfFieldCalculator lensInformation){
        return (((lensInformation.lens.getFocalLength()* lensInformation.lens.getFocalLength())/(double)(lensInformation.getUserEnteredAperture()*lensInformation.getCOC()))/(double)1000);
    }

    public double NFP(DepthOfFieldCalculator lensInformation){
        this.NFP = ((hyperFocalDistanceCalculator(lensInformation) * (double)lensInformation.getUserEnteredDistanceToSubject()*1000000)/((double)(hyperFocalDistanceCalculator(lensInformation)*1000 + ((lensInformation.getUserEnteredDistanceToSubject()*1000) - lensInformation.lens.getFocalLength()))));

        return this.NFP/(double)1000;
    }

    public double FFP(DepthOfFieldCalculator lens){

        if(hyperFocalDistanceCalculator(lens) > lens.getUserEnteredDistanceToSubject()) {
            this.FFP = ((hyperFocalDistanceCalculator(lens) * (double) lens.getUserEnteredDistanceToSubject() * 1000000) / ((double) (hyperFocalDistanceCalculator(lens) * 1000 - ((lens.getUserEnteredDistanceToSubject() * 1000) - lens.lens.getFocalLength()))));
            return this.FFP/(double)1000;
        }else{
            return Double.POSITIVE_INFINITY;
        }

    }

    public double DoFCalculator(DepthOfFieldCalculator lens){

        return (FFP(lens) - NFP(lens));
    }


}