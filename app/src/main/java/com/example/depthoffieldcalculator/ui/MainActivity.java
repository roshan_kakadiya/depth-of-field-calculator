package com.example.depthoffieldcalculator.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.example.depthoffieldcalculator.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import com.example.depthoffieldcalculator.model.Lens;
import com.example.depthoffieldcalculator.model.LensManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {
    private LensManager lenses;

    @Override
    protected void onCreate(Bundle save) {
        super.onCreate(save);
        setContentView(R.layout.activity_main);
        Toolbar temp = findViewById(R.id.temp);
        setSupportActionBar(temp);

        lenses = LensManager.getInstance();
        populateListView();
        registerCallBack();


        FloatingActionButton f = findViewById(R.id.create);
        f.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent intent = LensActivity.makeLaunchIntent(MainActivity.this);
                startActivityForResult(intent, 33);
            }
        });

    }


    private void populateListView() {
        populateLensList();
        ArrayAdapter<Lens> adapter = new MyListAdapter();
        ListView list = (ListView) findViewById(R.id.lensListView);
        list.setAdapter(adapter);

    }

    private void populateLensList() {

        lenses.add(new Lens("Canon", 1.8,50));
        lenses.add(new Lens("Tamron",2.8,90));
        lenses.add(new Lens("Sigma",2.8,200));
        lenses.add(new Lens("Nikon", 4.0,200));
    }


    private void registerCallBack() {
        ListView templist = (ListView) findViewById(R.id.lensListView);
        templist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int pos, long id) {

                for(int i = 0 ; i < 50;i++){
                    if (pos == i){
                        Lens selectedLens = lenses.getLens().get(i);
                        Intent intent = CalculatorDoF.makeLaunchIntent(MainActivity.this,selectedLens);
                        startActivity(intent);
                    }
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int coderequest, int coderesult, @Nullable Intent data) {

        if (coderesult == Activity.RESULT_CANCELED){
            Toast.makeText(getBaseContext(),"User Canceled",Toast.LENGTH_LONG).show();
            return;
        }

        switch (coderequest){
            case 33:
                if (coderesult == Activity.RESULT_OK){

                    String newstring = data.getStringExtra("Name of lens");
                    int newFocalLength = data.getIntExtra("Focal length of lens",0);
                    double newAperture = data.getDoubleExtra("Aperture of lens",0);
                    Lens len = new Lens(newstring,newAperture,newFocalLength);
                    lenses.add(len);

                    ArrayAdapter<Lens> adp = new MyListAdapter();
                    ListView newlist = (ListView) findViewById(R.id.lensListView);
                    newlist.setAdapter(adp);
                }

        }
    }






    private class MyListAdapter extends ArrayAdapter<Lens> {


        public MyListAdapter(){
            super(MainActivity.this, R.layout.the_items,lenses.getLens());
        }

        @Override
        public View getView(int index, @Nullable View convertView, @NonNull ViewGroup parent) {
            // We make sure to have a view to work with.
            View lensView = convertView;
            if (lensView == null){
                lensView = getLayoutInflater().inflate(R.layout.the_items, parent,false);
            }
            Lens currentLens = lenses.getLens().get(index);

            TextView lensMake = (TextView) lensView.findViewById(R.id.lensInformation);
            lensMake.setText(currentLens.getMake() + " " + (int) currentLens.getFocalLength() + "mm F" + currentLens.getMaxAperture());
            return lensView;
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu m) {

        getMenuInflater().inflate(R.menu.menu_main, m);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();


        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
