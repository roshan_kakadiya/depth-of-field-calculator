package com.example.depthoffieldcalculator.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.example.depthoffieldcalculator.R;
import com.example.depthoffieldcalculator.model.DepthOfFieldCalculator;

import com.example.depthoffieldcalculator.model.Lens;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;

public class CalculatorDoF extends AppCompatActivity {

    private static final String EXTRA_MAKE = "extra_make";
    private static final String EXTRA_FOCAL_LENGTH = "extra focal length";
    private static final String EXTRA_APERTURE = "extra aperture";
    private Lens lens;

    private String formatM(double distanceInM){
        DecimalFormat df = new DecimalFormat("0.00");
        return df.format(distanceInM);
    }

    @Override
    protected void onCreate(Bundle save) {
        super.onCreate(save);
        setContentView(R.layout.calculate_ui);
        extractDataFromIntent();
        setUpCameraMake();
        calculateButton();
        cancelButton();
    }

    private void cancelButton() {
        FloatingActionButton backButton = (FloatingActionButton) findViewById(R.id.backButtonofcalculateDof);
        backButton.setOnClickListener(v -> {
            finish();
        });
    }



    private void calculateButton() {
        Button calculateButton = (Button) findViewById(R.id.calculatebutton);
        calculateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText edit = (EditText) findViewById(R.id.dist);
                String message = edit.getText().toString();
                double userEnteredDistSubject = Double.parseDouble(message);
                if(userEnteredDistSubject < 0){
                    Toast.makeText(getBaseContext(),"Distance can't be negative", Toast.LENGTH_LONG).show();
                    return;
                }

                EditText edit1 = (EditText) findViewById(R.id.aperture);
                String message1 = edit1.getText().toString();
                double Aperture  = Double.parseDouble(message1);
                if(Aperture < 1.4){
                    Toast.makeText(getBaseContext(),"Aperture has to be greater than 1.4", Toast.LENGTH_LONG).show();
                    return;
                }

                TextView nearFocal = (TextView) findViewById(R.id.nearFocalAns);
                TextView farFocal = (TextView) findViewById(R.id.farFocalAns);
                TextView depthOfField = (TextView) findViewById(R.id.depthOfFieldAns);
                TextView hyperFocal = (TextView)findViewById(R.id.hyperFocalDistAns);

                if(Aperture < lens.getMaxAperture()){
                    nearFocal.setText("Invalid aperture");
                    farFocal.setText("Invalid aperture");
                    depthOfField.setText("Invalid aperture");
                    hyperFocal.setText("Invalid aperture");
                }else{
                    DepthOfFieldCalculator LensObject = new DepthOfFieldCalculator(lens,userEnteredDistSubject,Aperture,0.029);
                    nearFocal.setText(""+ formatM(LensObject.NFP(LensObject))+"m");
                    farFocal.setText(""+formatM(LensObject.FFP(LensObject))+"m");
                    depthOfField.setText(""+formatM(LensObject.DoFCalculator(LensObject))+"m");
                    hyperFocal.setText(""+formatM(LensObject.hyperFocalDistanceCalculator(LensObject))+"m");
                }

            }
        });
    }

    private void setUpCameraMake() {
        TextView tv1 = (TextView) findViewById(R.id.lensInfoActivity);
        tv1.setText("" + lens.getMake() + " " + (int)lens.getFocalLength() + "mm F" + lens.getMaxAperture());
    }

    public static Intent makeLaunchIntent(Context c, Lens lens){
        Intent intent = new Intent(c, CalculatorDoF.class);
        intent.putExtra(EXTRA_MAKE,lens.getMake());
        intent.putExtra(EXTRA_FOCAL_LENGTH,lens.getFocalLength());
        intent.putExtra(EXTRA_APERTURE,lens.getMaxAperture());
        return intent;
    }

    private void extractDataFromIntent() {

        Intent intent = getIntent();
        String make = intent.getStringExtra(EXTRA_MAKE);
        double focalLength = intent.getDoubleExtra(EXTRA_FOCAL_LENGTH,0);
        double aperture = intent.getDoubleExtra(EXTRA_APERTURE,0);
        lens = new Lens(make,aperture,(int)focalLength);
    }
}
