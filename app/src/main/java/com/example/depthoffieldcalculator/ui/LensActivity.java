package com.example.depthoffieldcalculator.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.depthoffieldcalculator.R;


public class LensActivity extends AppCompatActivity {
    private EditText makeSave;
    private EditText apertureSave;
    private EditText focalSave;

    @Override
    protected void onCreate(Bundle save) {
        super.onCreate(save);
        setContentView(R.layout.activity_lens);
        Toolbar temp = findViewById(R.id.temp);
        setSupportActionBar(temp);
        saveButton();
        cancelButton();
    }

    private void saveButton(){
        Button saveButton = (Button) findViewById(R.id.saveButton);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText make = (EditText) findViewById(R.id.makelens);
                String lensMake = make.getText().toString();
                if(lensMake.trim().equals("")){
                    Toast.makeText(getBaseContext(),"Name can't be empty!", Toast.LENGTH_LONG).show();
                    return;
                }
                EditText focalLengthAns = (EditText) findViewById(R.id.focalLengthlens);
                String focalLens = focalLengthAns.getText().toString();
                int focalLensAns = Integer.parseInt(focalLens);
                if(focalLensAns < 0){
                    Toast.makeText(getBaseContext(),"Focal Length Cant be Negative!",Toast.LENGTH_LONG).show();
                    return;
                }
                EditText apertureLens = (EditText) findViewById(R.id.aperturelens);
                String apertureLensAns = apertureLens.getText().toString();
                double newAperture = Double.parseDouble(apertureLensAns);
                if (newAperture < 1.4){
                    Toast.makeText(getBaseContext(),"Aperture Size Cant be < 1.4 !",Toast.LENGTH_LONG).show();
                    return;
                }
                Intent intent = new Intent();
                intent.putExtra("Name of lens", lensMake);
                intent.putExtra("Focal length of lens",focalLensAns);
                intent.putExtra("Aperture of lens",newAperture);
                setResult(Activity.RESULT_OK,intent);
                finish();
            }
        });
    }


    private void cancelButton() {
        FloatingActionButton cancelButton = findViewById(R.id.backButtonLensActivity);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v){
                Intent intent = new Intent();
                setResult(Activity.RESULT_CANCELED,intent);
                finish();
            }
        });
    }

    public static Intent makeLaunchIntent(Context c){
        Intent intent = new Intent(c, LensActivity.class);
        return intent;
    }




}